# the user for this project is yash001 and password is same as earlier

from django.db import models

class Item(models.Model):
    name        = models.CharField(max_length = 150) 
    description = models.TextField()
    
    def __str__(self):         # to get the title name instead of object1
        return self.name[:50]  # we mentioned name above so we need to mention only that here also
