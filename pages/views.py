from django.views.generic import ListView
from .models import Item

class HomePageView(ListView):
    model               = Item
    template_name       = 'home.html'      # access the contents of this file
    context_object_name = 'all_post_list'  # description below
    #context_object_name = 'object_list'   #  in actual we use this
'''
Note:
here i used context_object_name to print those new entries which i created in with the help of
models (by clicking on add on web page) on the admin page. 
In short with the content of html file all the entries made by the user will be displayed on the 
home page.
'''