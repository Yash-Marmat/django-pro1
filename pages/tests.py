from django.test import TestCase 
from django.urls import reverse

from .models import Item

class ItemModelTest(TestCase):         # Important

    def setUp(self):                   # summary below
        Item.objects.create(name = 'just a test') 

    def test_name_content(self):                                 # changes
     #  test_ <name of the field or attribute present in your model class>
        item = Item.objects.get(id=1)                            # changes
        expected_object_name = f'{item.name}'                    # changes
        self.assertEqual(expected_object_name, 'just a test')

class ItemTestSecond(TestCase):

    def setUp(self):                   # summary below
        Item.objects.create(description = 'just a test') 

    def test_description_content(self):
        item = Item.objects.get(id=1)
        expected_object_name = f'{item.description}'
        self.assertEqual(expected_object_name, 'just a test')

class HomePageViewTest(TestCase):
    def setUp(self):
        Item.objects.create(name = 'location test')

    def test_view_url_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_name(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_templates(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'home.html')



'''
def setUp:
Let’s start by adding a sample post to the text database field and then check that it
is stored correctly in the database. It’s important that all our test methods start with
test_ so Django knows to test them! 

Let’s start by adding a sample post to the text database field and then check that it
is stored correctly in the database. It’s important that all our test methods start with
test_ so Django knows to test them! 
'''